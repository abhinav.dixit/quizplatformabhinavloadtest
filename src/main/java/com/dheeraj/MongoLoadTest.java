package com.dheeraj;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.InsertOneResult;
import org.apache.log4j.*;
import org.bson.Document;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Hello world!
 */

@FunctionalInterface
interface MongoDataGeneratorVerify<T> {
    T uploadAndVerify(MongoDatabase mongoDB, String threadName, int userCount, int upsertCount);
}

class MongoLoadTestInfo {
    int insertCount;
    int upsertCount;
    int verifyCount;
    int upsertSuccess;
    int verifySuccess;

    public MongoLoadTestInfo(int insertCount, int upsertCount, int verifyCount, int upsertSuccess, int verifySuccess) {
        this.insertCount = insertCount;
        this.upsertCount = upsertCount;
        this.verifyCount = verifyCount;
        this.upsertSuccess = upsertSuccess;
        this.verifySuccess = verifySuccess;
    }

    public MongoLoadTestInfo() {
    }
}

class MongoInsertResult {
    String id;
    boolean success;

    public MongoInsertResult(String id, boolean success) {
        this.id = id;
        this.success = success;
    }
}

class MongoHelper {
    public static final Map<String, WriteConcern> writeConcernMap = new HashMap<>();
    public static final Map<String, ReadConcern> readConcernMap = new HashMap<>();
    public static final Map<String, ReadPreference> readPreferenceMap = new HashMap<>();
//    public static final String MONGODB_URL = "mongodb+srv://admin:admin@quiz.zp2mk.mongodb.net/?maxIdleTimeMS=10000&maxPoolSize=30000";
    public static final String MONGODB_URL = "mongodb+srv://quiz_admin:Password123@quiz-platform.iytb2.mongodb.net/?maxIdleTimeMS=10000&maxPoolSize=30000";
    public static final String MONGODB_DATABASE = "quiz";
    public static final String USER_QUIZ_COLLECTION = "user_quiz";

    public static final String QUIZ_ID = "1becd62e-abf6-4654-bc67-e950b80e9490";
    public static final String ANSWER_ID = "a43ac0aa-3ae2-12ye-a58a-7b3dedab131e";
    public static final String QUESTION_ID = "32523d4e-70ed-47ad-9354-0bfa6f88af74";
    public static final String TENANT_ID = "32523d4";
    public static final String QUIZ_TYPE = "POST_CLASS";
    public static final String STATUS = "assigned";

    public static MongoClient getMongoConnection(String WC, Boolean JOURNALING, String RC, String RP) throws Exception {

        if (! writeConcernMap.containsKey(WC)) {
            throw new Exception("WC not found");
        }

        if (! readConcernMap.containsKey(RC)) {
            throw new Exception("RC not found");
        }

        if (! readPreferenceMap.containsKey(RP)) {
            throw new Exception("RP not found");
        }


        WriteConcern writeConcern = JOURNALING ? writeConcernMap.get(WC).withJournal(true) : writeConcernMap.get(WC) ;
        ReadConcern readConcern = readConcernMap.get(RC);
        ReadPreference readPreference = readPreferenceMap.get(RP);


        return MongoClients.create(
                MongoClientSettings.
                        builder().
                        applyConnectionString(new ConnectionString(MongoHelper.MONGODB_URL)).
                        readConcern(readConcern).
                        writeConcern(writeConcern).
                        readPreference(readPreference).
                        build()
        );
    }

    public static MongoInsertResult insertUser(Logger logger, MongoDatabase mongoDatabase, String userId) {
        long duration = 0;
        try {
            Document filters = new Document();
            filters.append("user_id", userId)
                    .append("quiz_id", MongoHelper.QUIZ_ID)
                    .append("tenant_id", MongoHelper.TENANT_ID)
                    .append("quiz_type", MongoHelper.QUIZ_TYPE)
                    .append("assigned_status", MongoHelper.STATUS)
                    .append("created_on", new Date(System.currentTimeMillis()))
                    .append("attempt_no", 1);

            Instant upsertStart = Instant.now();
            InsertOneResult result = mongoDatabase.getCollection(MongoHelper.USER_QUIZ_COLLECTION).insertOne(
                    filters
            );

            duration = Duration.between(upsertStart, Instant.now()).toMillis();
            logger.info(String.format("INSERT_USER,%s,%b,%d,%s", userId, true, duration, ""));
            return new MongoInsertResult(result.getInsertedId().asObjectId().getValue().toString(), true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.info(String.format("INSERT_USER,%s,%b,%d,%s", userId, false, duration, e.getMessage()));
            System.out.printf("INSERT_USER,%s,%b,%d,%s\n", userId, false, duration, e.getMessage());
            return new MongoInsertResult("", false);
        }
    }

    public static boolean updateData(Logger logger, MongoDatabase mongoDatabase, String _id, String userId, String quizId, String questionId) {
        boolean upsertSuccessFlag= false;
        long duration = 0;
        try {
            Document filters = new Document();
            filters.append("user_id", userId);
            filters.append("quiz_id", quizId);

            UpdateOptions options = new UpdateOptions().upsert(false);

            Document updates = new Document();
            updates.append("$addToSet",
                    new Document("answers", new Document("question_id", questionId)
                            .append("created_on", new Date(System.currentTimeMillis()))
                            .append("answer", ANSWER_ID)
                    ));

            Instant upsertStart = Instant.now();
            mongoDatabase.
                    getCollection(MongoHelper.USER_QUIZ_COLLECTION).
                    updateOne(
                            filters,
                            updates,
                            options
                    );
            duration = Duration.between(upsertStart, Instant.now()).toMillis();

            upsertSuccessFlag=true;
            logger.info(String.format("UPSERT_USER,%s,%b,%d,%s", _id, upsertSuccessFlag, duration, ""));
            return upsertSuccessFlag;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.info(String.format("UPSERT_USER,%s,%b,%d,%s", _id, upsertSuccessFlag, duration, e.getMessage()));
            return upsertSuccessFlag;
        }
    }

    public static boolean verifyData(Logger logger1, MongoDatabase mongoDatabase, String _id, String userId, String quizId, int questionCount) {
        boolean verify;
        long duration = 0;
        try {
            Document filters = new Document();
            filters.append("user_id", userId);
            filters.append("quiz_id", quizId);

            Instant verifyStart = Instant.now();
            Document data = mongoDatabase.getCollection(MongoHelper.USER_QUIZ_COLLECTION).find(filters).first();
            duration = Duration.between(verifyStart, Instant.now()).toMillis();

            int size = data.getList("answers", Document.class).size();
            verify = size == questionCount;
            logger1.info(String.format("VERIFY_USER,%s,%b,%d,%s", _id, verify, duration,""));
            return verify;
        } catch (Exception e) {
            logger1.error(e.getMessage());
            verify =  false;
            logger1.info(String.format("VERIFY_USER,%s,%b,%d,%s", _id, verify, duration, e.getMessage()));
            return verify;
        }
    }

    static {
        writeConcernMap.put("w1", WriteConcern.W1);
        writeConcernMap.put("w2", WriteConcern.W2);
        writeConcernMap.put("majority", WriteConcern.MAJORITY);

        readConcernMap.put("local", ReadConcern.LOCAL);
        readConcernMap.put("majority", ReadConcern.MAJORITY);
        readConcernMap.put("linearizable", ReadConcern.LINEARIZABLE);

        readPreferenceMap.put("primary", ReadPreference.primary());
        readPreferenceMap.put("primaryPreferred", ReadPreference.primaryPreferred());
        readPreferenceMap.put("secondary", ReadPreference.secondary()) ;
        readPreferenceMap.put("secondaryPreferred", ReadPreference.secondaryPreferred()) ;
    }
}



public class MongoLoadTest {
    public static void main(String[] args) throws Exception {
        final int THREAD_COUNT = Integer.parseInt(args[0]);
        final int USERS_THREAD_COUNT =Integer.parseInt(args[1]);
        final int UPSERT_COUNT_USER = Integer.parseInt(args[2]);
        final boolean VERIFY_DATA = Boolean.parseBoolean(args[3].split("-")[0]);
        final boolean SINGLE_VERIFY = Boolean.parseBoolean(args[3].split("-")[1]);
        final boolean JOURNALING = Boolean.parseBoolean(args[4]);
        final String WC = args[5];
        final String RC = args[6];
        final String RP = args[7];
        final String CLUSTER_INFO = args.length > 8 ? (args[8]) : "NA";
        final String LOG_FILE = args.length > 9 ? args[9] : String.format(
                "mongo_lt_%d_%d_%d_%s_%s_%s_%s_%d.log", THREAD_COUNT, USERS_THREAD_COUNT, UPSERT_COUNT_USER, WC, RC, RP, CLUSTER_INFO,Calendar.getInstance().getTimeInMillis()
        );

        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile("./" + LOG_FILE);
        fa.setLayout(new PatternLayout("%m%n"));
        fa.setThreshold(Level.DEBUG);
        fa.setAppend(true);
        fa.activateOptions();

        final Logger logger = LogManager.getLogger(MongoLoadTest.class);
        BasicConfigurator.configure(fa);

        final String prefix = UUID.randomUUID().toString();

        MongoClient client = MongoHelper.getMongoConnection(WC, JOURNALING, RC, RP);
        MongoDatabase mongoDB = client.getDatabase(MongoHelper.MONGODB_DATABASE);

        MongoDataGeneratorVerify<MongoLoadTestInfo> verify = (mongoDatabase, threadName, userCount, upsertCount) -> {
            int insertCount = 0;
            int upsertRecordCount = 0;
            int verifyRecordCount = 0;
            int upsertRecordSuccess = 0;
            int verifyRecordSuccess = 0;

            for (int i = 1; i <= userCount; i++) {
                String userId = String.format("%s_%s_User-%s", prefix, threadName, i);
                MongoInsertResult result = MongoHelper.insertUser(logger, mongoDatabase, userId);
                insertCount++;
                if (result.success) {
                    for (int j = 1; j <= upsertCount; j++) {
                        String questionId = String.format("%s_%s_%s_%s", prefix, threadName, j, MongoHelper.QUESTION_ID);
                        upsertRecordCount++;
                        boolean upsertSuccessFlag = MongoHelper.updateData(logger, mongoDB, result.id, userId, MongoHelper.QUIZ_ID, questionId);
                        if (upsertSuccessFlag) {
                            upsertRecordSuccess++;
                            if (VERIFY_DATA && !SINGLE_VERIFY ) {
                                verifyRecordCount++;
                                boolean verifyFlag = MongoHelper.verifyData(logger, mongoDB, result.id, userId, MongoHelper.QUIZ_ID, j);
                                if (verifyFlag) {
                                    verifyRecordSuccess++;
                                }
                            }
                        }
                    }
                    if (VERIFY_DATA && SINGLE_VERIFY ) {
                        verifyRecordCount++;
                        boolean verifyFlag = MongoHelper.verifyData(logger, mongoDB, result.id, userId, MongoHelper.QUIZ_ID, upsertCount);
                        if (verifyFlag) {
                            verifyRecordSuccess++;
                        }
                    }
                }
            }

            System.out.printf("Completed Thread : %s\n", threadName);
            return new MongoLoadTestInfo(insertCount, upsertRecordCount, verifyRecordCount, upsertRecordSuccess, verifyRecordSuccess);
        };

        ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

        Instant start = Instant.now();
        List<CompletableFuture<MongoLoadTestInfo>> futures = new ArrayList<>();
        for (int i = 1; i <= THREAD_COUNT; i++) {
            String threadName = String.format("Th-%d", i);
            System.out.println((String.format("Started Thread : %s", threadName)));
            CompletableFuture<MongoLoadTestInfo> future = CompletableFuture.supplyAsync(() -> verify.uploadAndVerify(mongoDB, threadName, USERS_THREAD_COUNT, UPSERT_COUNT_USER), es);
            futures.add(future);
        }

        futures.forEach(CompletableFuture::join);
        MongoLoadTestInfo testInfo = new MongoLoadTestInfo();
        for (CompletableFuture<MongoLoadTestInfo> future : futures) {
            MongoLoadTestInfo futureInfo = future.get();
            testInfo.upsertCount += futureInfo.upsertCount;
            testInfo.verifyCount += futureInfo.verifyCount;
            testInfo.upsertSuccess += futureInfo.upsertSuccess;
            testInfo.verifySuccess += futureInfo.verifySuccess;
            testInfo.insertCount += futureInfo.insertCount;
        }
        long totalDuration = Duration.between(start, Instant.now()).getSeconds();
        es.shutdown();

        System.out.printf("File Name : %s %n", LOG_FILE);
        System.out.printf("Prefix : %s %n", prefix);
        System.out.printf("Test Started : %s %n", LocalDateTime.now());
        System.out.printf("Test Ended : %s %n", LocalDateTime.now());
        System.out.printf("Total duration : %d %n", totalDuration);
        System.out.printf("Total Insert Count : %d %n", testInfo.insertCount);
        System.out.printf("Total Upsert Count : %d %n", testInfo.upsertCount);
        System.out.printf("Total Verify Count : %d %n", testInfo.verifyCount);
        System.out.printf("Total Upsert Success count : %d %n", testInfo.upsertSuccess);
        System.out.printf("Total Verify Success count : %d %n", testInfo.verifySuccess);
        System.out.printf("QPS : %d %n", (testInfo.upsertCount + testInfo.verifyCount + testInfo.insertCount) / totalDuration);
        client.close();
    }
}
