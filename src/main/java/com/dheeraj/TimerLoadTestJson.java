package com.dheeraj;


import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.log4j.*;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

@Setter
@Getter
@AllArgsConstructor
class SectionPropertiesDto{
    String sectionId;
    List<String> questionIds;
    String status;
    Date startTime;
    Date EndTime;
}

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
class SectionPropertiesJson{
    List<SectionPropertiesDto> section_properties;
}


class TimerHelperJson extends TimerHelper{


    public static String getSectionJson(int no_sections){
        int questionCount = 10;
        String questionId_format = "q_%d";
        List<List<String>>sectionQuestionIds = new ArrayList<>();
        for(int j =1 ;j <=no_sections;++j){
            List<String> questionIds = new ArrayList<>();
            for(int i=1;i<=questionCount;++i){
                questionIds.add(String.format(questionId_format,ScyllaHelper.getRandomValue(1,100)));
            }
            sectionQuestionIds.add(questionIds);
        }
        Date currDate = new Date(System.currentTimeMillis());
        List<SectionPropertiesDto> sectionPropertiesDtoList =new ArrayList<>();
        for(int i = 0;i<no_sections;++i){
            String sectionId = String.format(sectionId_format,i+1);

            SectionPropertiesDto section = new SectionPropertiesDto(sectionId,sectionQuestionIds.get(i),userSectionStatus.get(0),currDate,null);
            sectionPropertiesDtoList.add(section);
        }
        SectionPropertiesJson  sectionPropertiesJson = new SectionPropertiesJson(sectionPropertiesDtoList);
        return new Gson().toJson(sectionPropertiesJson);
    }

    public static SectionPropertiesJson getSectionProperties(int no_sections){
        int questionCount = 10;
        String questionId_format = "q_%d";
        List<List<String>>sectionQuestionIds = new ArrayList<>();
        for(int j =1 ;j <=no_sections;++j){
            List<String> questionIds = new ArrayList<>();
            for(int i=1;i<=questionCount;++i){
                questionIds.add(String.format(questionId_format,ScyllaHelper.getRandomValue(1,100)));
            }
            sectionQuestionIds.add(questionIds);
        }
        Date currDate = new Date(System.currentTimeMillis());
        List<SectionPropertiesDto> sectionPropertiesDtoList =new ArrayList<>();
        for(int i = 0;i<no_sections;++i){
            String sectionId = String.format(sectionId_format,i+1);

            SectionPropertiesDto section = new SectionPropertiesDto(sectionId,sectionQuestionIds.get(i),userSectionStatus.get(0),currDate,null);
            sectionPropertiesDtoList.add(section);
        }
        return new SectionPropertiesJson(sectionPropertiesDtoList);
    }

    public SectionPropertiesJson readSectionPropertiesJson(String data){
        return new Gson().fromJson(data,SectionPropertiesJson.class);
    }

    public static ScyllaInsertResult insertUser(Logger logger, Session session, String userId,String quizId, int totalQuestions, int correctlyAnswered, int no_of_sections, ConsistencyLevel consistencyLevel) {
        long duration = 0;
        try {
            Instant insertStart = Instant.now();

            Statement insertQuery = QueryBuilder.insertInto(ScyllaHelper.KEYSPACE, ScyllaHelper.USER_QUIZ_TABLE)
                    .value("user_id", userId)
                    .value("quiz_id", quizId)
                    .value("attempt_number", 1)
                    .value("tenant_id", ScyllaHelper.TENANT_ID)
                    .value("quiz_type", ScyllaHelper.QUIZ_TYPE)
                    .value("quiz_status", ScyllaHelper.getStatus())
                    .value("evaluation_status", "EVALUATED")
                    .value("rating", 5)
                    .value("feedback", "Dummy Feedback")
                    .value("total_questions", totalQuestions)
                    .value("incorrect_answers", totalQuestions - correctlyAnswered)
                    .value("partially_correct_answers", 0)
                    .value("correct_answers", correctlyAnswered)
                    .value("start_time", new Date(System.currentTimeMillis()))
                    .value("end_time", new Date(System.currentTimeMillis()))
                    .value("created_at", new Date(System.currentTimeMillis()))
                    .value("updated_at", new Date(System.currentTimeMillis()))
                    .value("question_ids", Arrays.asList("q1", "q2", "q3"))
                    .value("section_properties",getSectionJson(no_of_sections))
                    .value("updated_at_pk", ScyllaHelper.getPartitionKey(15))
                    .setConsistencyLevel(consistencyLevel);

            ResultSet resultSet = session.execute(insertQuery);

            duration = Duration.between(insertStart, Instant.now()).toMillis();
            logger.info(String.format("INSERT_USER,%s,%s,%b,%d,%s", userId,quizId, true, duration, ""));
            return new ScyllaInsertResult(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            System.out.printf("INSERT_USER,%s,%s,%b,%d,%s\n", userId,quizId, false, duration, e.getMessage());
            logger.info(String.format("INSERT_USER,%s,%s,%b,%d,%s", userId,quizId, false, duration, e.getMessage()));
            return new ScyllaInsertResult(false);
        }
    }

    public static boolean UserSectionRead(Logger logger1,Session session,String userId,String quizId, int attemptNo,int sectionNo,ConsistencyLevel consistencyLevel){
        String section_id = String.format(sectionId_format,sectionNo);
        boolean verify=false;
        long duration = 0;
        try {

            Instant verifyStart = Instant.now();
            Statement selectionQuery = QueryBuilder
                    .select()
                    .from(ScyllaHelper.KEYSPACE, TimerHelper.UserQuizSection)
                    .where(QueryBuilder.eq("user_id", userId))
                    .and(QueryBuilder.eq("quiz_id", quizId))
                    .and(QueryBuilder.eq("attempt_number", attemptNo))
                    .and(QueryBuilder.eq("section_id", section_id))
                    .setConsistencyLevel(consistencyLevel);

            ResultSet resultSet = session.execute(selectionQuery);
            int size = 0;
            List<Row> res = resultSet.all();
            if (res != null && res.size() > 0) {
                size = res.size();
            }
            duration = Duration.between(verifyStart, Instant.now()).toMillis();
            if (size == 1) {
                verify = true;
            }
            logger1.info(String.format("READ_USER_SECTION,%s,%s,%s,%d,%b,%s", userId, quizId,section_id,duration, verify,""));
            return verify;
        } catch (Exception e) {
            logger1.error(e.getMessage());
            verify =  false;
            logger1.info(String.format("READ_USER_SECTION,%s,%s,%s,%d,%b,%s", userId, quizId,section_id,duration, verify, e.getMessage()));
            return verify;
        }
    }

    public static boolean UserSectionUpdate(Logger logger, Session session, String userId, String quizId, int attemptNo,int sectionNo_start,int section_no,int noOfSections,ConsistencyLevel consistencyLevel){

        String sectionId = String.format(sectionId_format,section_no);
        Date currDate = new Date(System.currentTimeMillis());
        boolean upsertSuccessFlag= false;
        long duration = 0;
        try {
            SectionPropertiesJson sectionPropertiesJson = getSectionProperties(noOfSections);
            List<SectionPropertiesDto> sectionPropertiesDtoList = sectionPropertiesJson.getSection_properties();
            Map<String,SectionPropertiesDto> mp = sectionPropertiesDtoList.stream().collect(Collectors.toMap(SectionPropertiesDto::getSectionId, Function.identity(),(o, n)->n));
            List<SectionPropertiesDto> updatedSections = new ArrayList<>();
            Instant insertStart = Instant.now();
            for(int i = sectionNo_start;i<section_no; ++i){
                String id = String.format(sectionId_format,i);
                if(mp.containsKey(id)) {
                    SectionPropertiesDto sectionPropertiesDto = mp.get(id);
                    sectionPropertiesDto.setEndTime(currDate);
                    sectionPropertiesDto.setStatus(userSectionStatus.get(2));
                    updatedSections.add(sectionPropertiesDto);
                }
            }

            if(section_no<=noOfSections){
                if(mp.containsKey(sectionId)) {
                    SectionPropertiesDto sectionPropertiesDto = mp.get(sectionId);
                    sectionPropertiesDto.setEndTime(currDate);
                    sectionPropertiesDto.setStatus(userSectionStatus.get(1));
                    updatedSections.add(sectionPropertiesDto);
                }
            }
            sectionPropertiesJson.setSection_properties(updatedSections);
            Statement statement = QueryBuilder.update(ScyllaHelper.KEYSPACE,ScyllaHelper.USER_QUIZ_TABLE)
                    .with(QueryBuilder.set("section_properties", new Gson().toJson(sectionPropertiesJson)))
                    .where(QueryBuilder.eq("user_id", userId))
                    .and(QueryBuilder.eq("quiz_id", quizId))
                    .and(QueryBuilder.eq("attempt_number", attemptNo))
                    .setConsistencyLevel(consistencyLevel);
            session.execute(statement);

            duration = Duration.between(insertStart, Instant.now()).toMillis();
            upsertSuccessFlag = true;
            logger.info(String.format("UPDATE_USER_QUIZ_SECTION,%s,%s,%s,%d,%b,%s", userId,quizId,sectionId,duration ,upsertSuccessFlag , ""));
            return upsertSuccessFlag;
        } catch (Exception e) {
            logger.error(e.getMessage());
            System.out.printf("UPDATE_USER_QUIZ_SECTION,%s,%s,%s,%d,%b,%s\n", userId,quizId,sectionId,duration, upsertSuccessFlag, e.getMessage());
            logger.info(String.format("UPDATE_USER_QUIZ_SECTION,%s,%s,%s,%d,%b,%s", userId,quizId,sectionId,duration, upsertSuccessFlag,  e.getMessage()));
            return upsertSuccessFlag;
        }
    }

    public static Map<String,Object> getUserSectionMap(UserQuizSection userQuizSection){
        Map<String,Object> mp = new HashMap<>();
        mp.put("user_id",userQuizSection.getUserId());
        mp.put("quiz_id",userQuizSection.getQuizId());
        mp.put("attempt_number",userQuizSection.getAttemptNo());
        mp.put("section_id",userQuizSection.getSectionId());
        mp.put("question_ids",userQuizSection.getQuestionIds());
        mp.put("created_at",userQuizSection.getCreatedAt());
        mp.put("updated_at",userQuizSection.getUpdatedAt());
        mp.put("status",userQuizSection.getStatus());
        mp.put("start_time",userQuizSection.getStartTime());
        mp.put("end_time",userQuizSection.getEndTime());
        return  mp;

    }

    public static boolean verifyDataUserSection(Logger logger1, Session session, String userId, String quizId, int sectionCount,ConsistencyLevel consistencyLevel){
        boolean verify=false;
        long duration = 0;
        try {

            Instant verifyStart = Instant.now();
            Statement selectionQuery = QueryBuilder
                    .select()
                    .from(ScyllaHelper.KEYSPACE, TimerHelper.USER_QUIZ_TABLE)
                    .where(QueryBuilder.eq("user_id", userId))
                    .and(QueryBuilder.eq("quiz_id", quizId))
                    .and(QueryBuilder.eq("attempt_number", 1))
                    .setConsistencyLevel(consistencyLevel);

            ResultSet resultSet = session.execute(selectionQuery);
            int size = 0;
            List<Row> res = resultSet.all();
            if (res !=null && res.size() > 0){
                size = res.size();
            }
            duration = Duration.between(verifyStart, Instant.now()).toMillis();
            if (size == 1) {
                verify = true;
            }
            logger1.info(String.format("VERIFY_USER_SECTION,%s,%s,%b,%d,%s", userId, quizId, verify, duration,""));
            return verify;
        } catch (Exception e) {
            logger1.error(e.getMessage());
            verify =  false;
            logger1.info(String.format("VERIFY_USER_SECTION,%s,%s,%b,%d,%s", userId, quizId, verify, duration, e.getMessage()));
            return verify;
        }
    }



}

public class TimerLoadTestJson {
    public static void main(String[]args) throws Exception {
        final Map<String, ConsistencyLevel> consistencyLevelMap = new HashMap<String, ConsistencyLevel>(){
            {
                put("quorum", ConsistencyLevel.QUORUM);
            }
        };

        final int THREAD_COUNT = Integer.parseInt(args[0]);
        final int USERS_THREAD_COUNT =Integer.parseInt(args[1]);
        final int UPSERT_COUNT_USER = Integer.parseInt(args[2]);
        final boolean VERIFY_DATA = Boolean.parseBoolean(args[3].split("-")[0]);
        final boolean SINGLE_VERIFY = Boolean.parseBoolean(args[3].split("-")[1]);
        final String WC = args[4];
        final String RC = args[5];
        final int SECTION_COUNT = Integer.parseInt(args[6]);
        final String CLUSTER_INFO = args.length > 7 ? (args[7]) : "NA";
        final String LOG_FILE = args.length > 8 ? args[8] : String.format(
                "scylla_lt_%d_%d_%d_%d_%s_%s_%s_%d.log", THREAD_COUNT, USERS_THREAD_COUNT, UPSERT_COUNT_USER,SECTION_COUNT, WC, RC, CLUSTER_INFO, Calendar.getInstance().getTimeInMillis()
        );

        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile("./" + LOG_FILE);
        fa.setLayout(new PatternLayout("%m%n"));
        fa.setThreshold(Level.DEBUG);
        fa.setAppend(true);
        fa.activateOptions();

        final Logger logger = LogManager.getLogger(ScyllaLoadTest.class);
        BasicConfigurator.configure(fa);

        final String prefix = UUID.randomUUID().toString();

        Cluster cluster = ScyllaHelper.getConnection();
        Session session = cluster.connect();

        TimerDataGeneratorVerify<TimerLoadTestInfo> verify = (scyllaSession, threadName, userCount, upsertCount) -> {
            int insertCount = 0;
            int upsertRecordCount = 0;
            int verifyRecordCount = 0;
            int verifySectionRecordCount = 0;
            int upsertSectionRecordCount = 0;
            int upsertRecordSuccess = 0;
            int upsertSectionRecordSuccess = 0;
            int verifyRecordSuccess = 0;
            int verifySectionRecordSuccess = 0;

            for (int i = 1; i <= userCount; i++) {
                String userId = String.format("%s_%s_User%s", prefix, threadName, i);
                int correctCount = TimerHelperJson.getRandomValue(1,UPSERT_COUNT_USER);
                String quizId = TimerHelperJson.QUIZ_ID;
                ScyllaInsertResult result = TimerHelperJson.insertUser(logger, scyllaSession, userId,quizId, upsertCount, correctCount,SECTION_COUNT, consistencyLevelMap.get(WC));
                insertCount++;
                if (result.success) {
//                    upsertSectionRecordCount++;
//                    boolean upsertSectionSuccessFlag = TimerHelper.UserSectionInsert(logger,scyllaSession,userId,quizId,1,SECTION_COUNT,consistencyLevelMap.get(WC));
//                    if(upsertSectionSuccessFlag){
//                        upsertSectionRecordSuccess++;
                        for (int j = 1; j <= upsertCount; j++) {
                            String questionId = String.format("%s_%d", TimerHelperJson.QUESTION_ID, j);
                            // Add call to check section Update
                            boolean verifySectionFlag;
                            int sectionNo = (j/10 )+ 1;
                            if(j%10 == 0 || j==1){
                                System.out.printf("j %d   sectionNo %d\n",j,sectionNo);
                                verifySectionRecordCount++;
                                verifySectionFlag = TimerHelperJson.verifyDataUserSection(logger, scyllaSession, userId, quizId, SECTION_COUNT, consistencyLevelMap.get(RC));
                                if(verifySectionFlag){
                                    verifySectionRecordSuccess++;
                                }
                                upsertSectionRecordCount++;
                                int sectionStartNo = (sectionNo-1>0)?sectionNo-1:1;
                                boolean updateSectionProgressFlag = TimerHelperJson.UserSectionUpdate(logger,scyllaSession,userId,quizId,1,sectionStartNo,sectionNo,SECTION_COUNT,consistencyLevelMap.get(WC));
                                if(updateSectionProgressFlag){
                                    upsertSectionRecordSuccess++;
                                }
                            }
                            verifySectionRecordCount++;
                            verifySectionFlag = TimerHelperJson.verifyDataUserSection(logger, scyllaSession, userId, quizId,sectionNo, consistencyLevelMap.get(RC));
                            if(verifySectionFlag){
                                verifySectionRecordSuccess++;
                            }


                            upsertRecordCount++;
                            boolean upsertSuccessFlag = TimerHelperJson.updateData(logger, scyllaSession, userId, quizId, questionId, correctCount, j, consistencyLevelMap.get(WC));
                            if (upsertSuccessFlag) {
                                upsertRecordSuccess++;
                                if (VERIFY_DATA && !SINGLE_VERIFY ) {
                                    verifyRecordCount++;
                                    boolean verifyFlag = TimerHelperJson.verifyData(logger, scyllaSession, userId, quizId, j, consistencyLevelMap.get(RC));
                                    if (verifyFlag) {
                                        verifyRecordSuccess++;
                                    }
                                }
                            }
                        }
                        if (VERIFY_DATA && SINGLE_VERIFY ) {
                            verifyRecordCount++;
                            boolean verifyFlag = TimerHelperJson.verifyData(logger, scyllaSession, userId, ScyllaHelper.QUIZ_ID, upsertCount, consistencyLevelMap.get(RC));
                            if (verifyFlag) {
                                verifyRecordSuccess++;
                            }
                        }
                    }
                }

            System.out.printf("Completed Thread : %s\n", threadName);
            return new TimerLoadTestInfo(insertCount, upsertRecordCount, verifyRecordCount, upsertRecordSuccess, verifyRecordSuccess,upsertSectionRecordCount,upsertSectionRecordSuccess,verifySectionRecordCount,verifySectionRecordSuccess);
        };

        ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

        Instant start = Instant.now();
        List<CompletableFuture<TimerLoadTestInfo>> futures = new ArrayList<>();
        for (int i = 1; i <= THREAD_COUNT; i++) {
            String threadName = String.format("Th%d", i);
            System.out.println((String.format("Started Thread : %s", threadName)));
            CompletableFuture<TimerLoadTestInfo> future = CompletableFuture.supplyAsync(() -> verify.uploadAndVerify(session, threadName, USERS_THREAD_COUNT, UPSERT_COUNT_USER), es);
            futures.add(future);
        }

        futures.forEach(CompletableFuture::join);
        TimerLoadTestInfo testInfo = new TimerLoadTestInfo();
        for (CompletableFuture<TimerLoadTestInfo> future : futures) {
            TimerLoadTestInfo futureInfo = future.get();
            testInfo.upsertRecordCount += futureInfo.upsertRecordCount;
            testInfo.verifyRecordCount += futureInfo.verifyRecordCount;
            testInfo.upsertRecordSuccess += futureInfo.upsertRecordSuccess;
            testInfo.verifyRecordSuccess += futureInfo.verifyRecordSuccess;
            testInfo.insertCount += futureInfo.insertCount;
            testInfo.verifySectionRecordSuccess = futureInfo.verifySectionRecordSuccess;
            testInfo.upsertSectionRecordSuccess = futureInfo.upsertSectionRecordSuccess;
            testInfo.upsertSectionRecordCount = futureInfo.upsertSectionRecordCount;
            testInfo.verifySectionRecordCount = futureInfo.verifySectionRecordCount;
        }

        Instant end = Instant.now();
        long totalDuration = Duration.between(start, end).getSeconds();
        System.out.printf("File Name : %s %n", LOG_FILE);
        System.out.printf("Prefix : %s %n", prefix);
        System.out.printf("Question Id : %s %n", ScyllaHelper.QUESTION_ID);
        System.out.printf("Quiz Id : %s %n", ScyllaHelper.QUIZ_ID);
        System.out.printf("Test Started : %s %n", start);
        System.out.printf("Test Ended : %s %n", end);
        System.out.printf("Total duration : %d %n", totalDuration);
        System.out.printf("Total Insert Count : %d %n", testInfo.insertCount);
        System.out.printf("Total Upsert Count : %d %n", testInfo.upsertRecordCount);
        System.out.printf("Total Verify Count : %d %n", testInfo.verifyRecordCount);
        System.out.printf("Total Upsert Success count : %d %n", testInfo.upsertRecordSuccess);
        System.out.printf("Total Verify Success count : %d %n", testInfo.verifyRecordSuccess);
        System.out.printf("Total Section Upsert Count : %d %n", testInfo.upsertSectionRecordCount);
        System.out.printf("Total Section Verify Count : %d %n", testInfo.verifySectionRecordCount);
        System.out.printf("Total Section Upsert Success count : %d %n", testInfo.upsertSectionRecordSuccess);
        System.out.printf("Total Section Verify Success count : %d %n", testInfo.verifySectionRecordSuccess);
        System.out.printf("QPS : %d %n", totalDuration >0 ? (testInfo.upsertRecordCount + testInfo.verifyRecordCount+testInfo.upsertSectionRecordCount+ testInfo.verifySectionRecordCount + testInfo.insertCount) / totalDuration : (testInfo.upsertRecordCount + testInfo.verifyRecordCount+testInfo.upsertSectionRecordCount+ testInfo.verifySectionRecordCount + testInfo.insertCount));
        es.shutdown();
        cluster.close();
    }
}
