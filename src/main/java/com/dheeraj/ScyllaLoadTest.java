package com.dheeraj;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import org.apache.log4j.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Hello world!
 */

@FunctionalInterface
interface ScyllaDataGeneratorVerify<T> {
    T uploadAndVerify(Session session, String threadName, int userCount, int upsertCount);
}

class ScyllaLoadTestInfo {
    int insertCount;
    int upsertCount;
    int verifyCount;
    int upsertSuccess;
    int verifySuccess;

    public ScyllaLoadTestInfo(int insertCount, int upsertCount, int verifyCount, int upsertSuccess, int verifySuccess) {
        this.insertCount = insertCount;
        this.upsertCount = upsertCount;
        this.verifyCount = verifyCount;
        this.upsertSuccess = upsertSuccess;
        this.verifySuccess = verifySuccess;
    }

    public ScyllaLoadTestInfo() {
    }
}

class ScyllaInsertResult {
    boolean success;

    public ScyllaInsertResult(boolean success) {
        this.success = success;
    }
}

class ScyllaHelper {
    public static final String KEYSPACE = "quiz";
    public static final String USER_QUIZ_TABLE = "user_quiz";
    public static final String USER_SUBMISSION_TABLE = "user_quiz_submissions";

    public static final String QUIZ_ID = UUID.randomUUID().toString();
    public static final String ANSWER_ID = "7b3dedab131e";
    public static final String QUESTION_ID = UUID.randomUUID().toString();
    public static final String TENANT_ID = "WHJR";
    public static final String QUIZ_TYPE = "POST_CLASS";
    public static final String[] STATUS = {"COMPLETED", "IN_PROGRESS", "ASSIGNED"};
    public static final String[] CONTACT_POINTS = {"13.214.89.13","3.0.9.170","18.142.11.58"};
//    public static final String[] CONTACT_POINTS = {"172.30.0.11", "172.30.1.11", "172.30.2.11"};
    public static final String EVALUATION_INFO_TYPE = "evaluation_info" ;

    public static Cluster getConnection() throws Exception {
        PoolingOptions  options = new PoolingOptions();

        options.setMaxConnectionsPerHost(HostDistance.LOCAL, 5000);
        options.setMaxConnectionsPerHost(HostDistance.REMOTE, 5000);

        return Cluster.builder()
                .addContactPoints(CONTACT_POINTS)
                .withPort(9042)
                .withAuthProvider(new PlainTextAuthProvider("scylla", "c2hSTUR7dDnLF3i"))
                .withPoolingOptions(options)
                .build();

    }

    public static ScyllaInsertResult insertUser(Logger logger, Session session, String userId, int totalQuestions, int correctlyAnswered, UserType evaluationInfo,  ConsistencyLevel consistencyLevel) {
        long duration = 0;
        try {
            Instant insertStart = Instant.now();

            Statement insertQuery = QueryBuilder.insertInto(ScyllaHelper.KEYSPACE, ScyllaHelper.USER_QUIZ_TABLE)
                    .value("user_id", userId)
                    .value("quiz_id", ScyllaHelper.QUIZ_ID)
                    .value("attempt_number", 1)
                    .value("tenant_id", ScyllaHelper.TENANT_ID)
                    .value("quiz_type", ScyllaHelper.QUIZ_TYPE)
                    .value("quiz_status", ScyllaHelper.getStatus())
                    .value("evaluation_status", "EVALUATED")
                    .value("rating", 5)
                    .value("feedback", "Dummy Feedback")
                    .value("total_questions", totalQuestions)
                    .value("incorrect_answers", totalQuestions - correctlyAnswered)
                    .value("partially_correct_answers", 0)
                    .value("correct_answers", correctlyAnswered)
                    .value("start_time", new Date(System.currentTimeMillis()))
                    .value("end_time", new Date(System.currentTimeMillis()))
                    .value("created_at", new Date(System.currentTimeMillis()))
                    .value("updated_at", new Date(System.currentTimeMillis()))
                    .value("question_ids", Arrays.asList("q1", "q2", "q3"))
//                    .value("created_at_pk", ScyllaHelper.getPartitionKey(30))
                    .value("updated_at_pk", ScyllaHelper.getPartitionKey(15))
                    .setConsistencyLevel(consistencyLevel);

            ResultSet resultSet = session.execute(insertQuery);

            duration = Duration.between(insertStart, Instant.now()).toMillis();
            logger.info(String.format("INSERT_USER,%s,,%b,%d,%s", userId, true, duration, ""));
            return new ScyllaInsertResult(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            System.out.printf("INSERT_USER,%s,,%b,%d,%s\n", userId, false, duration, e.getMessage());
            logger.info(String.format("INSERT_USER,%s,,%b,%d,%s", userId, false, duration, e.getMessage()));
            return new ScyllaInsertResult(false);
        }
    }

    public static boolean updateData(Logger logger, Session session, String userId, String quizId, String questionId, int correctAnswers, int questionCounter, ConsistencyLevel consistencyLevel) {
        boolean upsertSuccessFlag= false;
        long duration = 0;
        try {
            String evaluation_result = questionCounter <= correctAnswers ? "CORRECT": "INCORRECT";
            Instant insertStart = Instant.now();
            Statement insertQuery = QueryBuilder.insertInto(ScyllaHelper.KEYSPACE, ScyllaHelper.USER_SUBMISSION_TABLE)
                    .value("user_id", userId)
                    .value("quiz_id", quizId)
                    .value("question_id", questionId)
                    .value("attempt_number", 1)
                    .value("question_attempt_number", 1)
                    .value("evaluated", false)
                    .value("evaluation_result", evaluation_result)
                    .value("answer_text", Arrays.asList(ANSWER_ID))
                    .value("option_ids", Arrays.asList(ANSWER_ID))
                    .value("time_taken", 15)
                    .value("created_at", new Date(System.currentTimeMillis()))
                    .value("updated_at", new Date(System.currentTimeMillis()))
//                    .value("created_at_pk", ScyllaHelper.getPartitionKey(30))
                    .value("updated_at_pk", ScyllaHelper.getPartitionKey(15))
                    .setConsistencyLevel(consistencyLevel);

            session.execute(insertQuery);

            duration = Duration.between(insertStart, Instant.now()).toMillis();
            upsertSuccessFlag = true;
            logger.info(String.format("INSERT_USER_SUBMISSION,%s,%s,%b,%d,%s", userId,questionId, upsertSuccessFlag, duration, ""));
            return upsertSuccessFlag;
        } catch (Exception e) {
            logger.error(e.getMessage());
            System.out.printf("INSERT_USER_SUBMISSION,%s,%s,%b,%d,%s\n", userId,questionId, upsertSuccessFlag, duration, e.getMessage());
            logger.info(String.format("INSERT_USER_SUBMISSION,%s,%s,%b,%d,%s", userId, questionId, upsertSuccessFlag, duration, e.getMessage()));
            return upsertSuccessFlag;
        }
    }

    public static boolean verifyData(Logger logger1, Session session, String userId, String quizId, int questionCount,ConsistencyLevel consistencyLevel) {
        boolean verify=false;
        long duration = 0;
        try {

            Instant verifyStart = Instant.now();
            Statement selectionQuery = QueryBuilder
                    .select()
                    .from(ScyllaHelper.KEYSPACE, ScyllaHelper.USER_SUBMISSION_TABLE)
                    .where(QueryBuilder.eq("user_id", userId))
                    .and(QueryBuilder.eq("quiz_id", quizId))
                    .and(QueryBuilder.eq("attempt_number", 1))
                    .setConsistencyLevel(consistencyLevel);

            ResultSet resultSet = session.execute(selectionQuery);
            int size = 0;
            List<Row> res = resultSet.all();
            if (res !=null && res.size() > 0){
                size = res.size();
            }
            duration = Duration.between(verifyStart, Instant.now()).toMillis();
            if (size == questionCount) {
                verify = true;
            }
            logger1.info(String.format("VERIFY_USER,%s,%s,%b,%d,%s", userId, quizId, verify, duration,""));
            return verify;
        } catch (Exception e) {
            logger1.error(e.getMessage());
            verify =  false;
            logger1.info(String.format("VERIFY_USER,%s,%s,%b,%d,%s", userId, quizId, verify, duration, e.getMessage()));
            return verify;
        }
    }

    public static int getRandomValue(int min, int max){
        return (int) (Math.random()*(max-min)) + min;
    }

    public static String getPartitionKey(int min){
        DateTime time = new DateTime();
        DateTime dateTime = time.toDateTime(DateTimeZone.UTC ).minusMinutes((min +(time.getMinuteOfHour() % 15)));
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:00.000");
        return dtfOut.print(dateTime);
    }



    public static String getStatus(){
        List<String> givenList = Arrays.asList(ScyllaHelper.STATUS);
        Random rand = new Random();
        return givenList.get(rand.nextInt(givenList.size()));
    }
}



public class ScyllaLoadTest {
    public static void main(String[] args) throws Exception {
        final Map<String, ConsistencyLevel> consistencyLevelMap = new HashMap<String, ConsistencyLevel>(){
            {
                put("quorum", ConsistencyLevel.QUORUM);
            }
        };

        final int THREAD_COUNT = Integer.parseInt(args[0]);
        final int USERS_THREAD_COUNT =Integer.parseInt(args[1]);
        final int UPSERT_COUNT_USER = Integer.parseInt(args[2]);
        final boolean VERIFY_DATA = Boolean.parseBoolean(args[3].split("-")[0]);
        final boolean SINGLE_VERIFY = Boolean.parseBoolean(args[3].split("-")[1]);
        final String WC = args[4];
        final String RC = args[5];
        final String CLUSTER_INFO = args.length > 6 ? (args[6]) : "NA";
        final String LOG_FILE = args.length > 7 ? args[7] : String.format(
                "scylla_lt_%d_%d_%d_%s_%s_%s_%d.log", THREAD_COUNT, USERS_THREAD_COUNT, UPSERT_COUNT_USER, WC, RC, CLUSTER_INFO, Calendar.getInstance().getTimeInMillis()
        );

        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile("./" + LOG_FILE);
        fa.setLayout(new PatternLayout("%m%n"));
        fa.setThreshold(Level.DEBUG);
        fa.setAppend(true);
        fa.activateOptions();

        final Logger logger = LogManager.getLogger(ScyllaLoadTest.class);
        BasicConfigurator.configure(fa);

        final String prefix = UUID.randomUUID().toString();

        Cluster cluster = ScyllaHelper.getConnection();
        UserType evaluationInfo = cluster.getMetadata().getKeyspace(ScyllaHelper.KEYSPACE).getUserType(ScyllaHelper.EVALUATION_INFO_TYPE);
        Session session = cluster.connect();

        ScyllaDataGeneratorVerify<ScyllaLoadTestInfo> verify = (scyllaSession, threadName, userCount, upsertCount) -> {
            int insertCount = 0;
            int upsertRecordCount = 0;
            int verifyRecordCount = 0;
            int upsertRecordSuccess = 0;
            int verifyRecordSuccess = 0;

            for (int i = 1; i <= userCount; i++) {
                String userId = String.format("%s_%s_User%s", prefix, threadName, i);
                int correctCount = ScyllaHelper.getRandomValue(1,UPSERT_COUNT_USER);
                ScyllaInsertResult result = ScyllaHelper.insertUser(logger, scyllaSession, userId, upsertCount, correctCount,evaluationInfo,  consistencyLevelMap.get(WC));
                insertCount++;
                if (result.success) {
                    for (int j = 1; j <= upsertCount; j++) {
                        String questionId = String.format("%s_%d", ScyllaHelper.QUESTION_ID, j);
                        upsertRecordCount++;
                        boolean upsertSuccessFlag = ScyllaHelper.updateData(logger, scyllaSession, userId, ScyllaHelper.QUIZ_ID, questionId, correctCount, j, consistencyLevelMap.get(WC));
                        if (upsertSuccessFlag) {
                            upsertRecordSuccess++;
                            if (VERIFY_DATA && !SINGLE_VERIFY ) {
                                verifyRecordCount++;
                                boolean verifyFlag = ScyllaHelper.verifyData(logger, scyllaSession, userId, ScyllaHelper.QUIZ_ID, j, consistencyLevelMap.get(RC));
                                if (verifyFlag) {
                                    verifyRecordSuccess++;
                                }
                            }
                        }
                    }
                    if (VERIFY_DATA && SINGLE_VERIFY ) {
                        verifyRecordCount++;
                        boolean verifyFlag = ScyllaHelper.verifyData(logger, scyllaSession, userId, ScyllaHelper.QUIZ_ID, upsertCount, consistencyLevelMap.get(RC));
                        if (verifyFlag) {
                            verifyRecordSuccess++;
                        }
                    }
                }
            }

            System.out.printf("Completed Thread : %s\n", threadName);
            return new ScyllaLoadTestInfo(insertCount, upsertRecordCount, verifyRecordCount, upsertRecordSuccess, verifyRecordSuccess);
        };

        ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

        Instant start = Instant.now();
        List<CompletableFuture<ScyllaLoadTestInfo>> futures = new ArrayList<>();
        for (int i = 1; i <= THREAD_COUNT; i++) {
            String threadName = String.format("Th%d", i);
            System.out.println((String.format("Started Thread : %s", threadName)));
            CompletableFuture<ScyllaLoadTestInfo> future = CompletableFuture.supplyAsync(() -> verify.uploadAndVerify(session, threadName, USERS_THREAD_COUNT, UPSERT_COUNT_USER), es);
            futures.add(future);
        }

        futures.forEach(CompletableFuture::join);
        ScyllaLoadTestInfo testInfo = new ScyllaLoadTestInfo();
        for (CompletableFuture<ScyllaLoadTestInfo> future : futures) {
            ScyllaLoadTestInfo futureInfo = future.get();
            testInfo.upsertCount += futureInfo.upsertCount;
            testInfo.verifyCount += futureInfo.verifyCount;
            testInfo.upsertSuccess += futureInfo.upsertSuccess;
            testInfo.verifySuccess += futureInfo.verifySuccess;
            testInfo.insertCount += futureInfo.insertCount;
        }

        Instant end = Instant.now();
        long totalDuration = Duration.between(start, end).getSeconds();
        System.out.printf("File Name : %s %n", LOG_FILE);
        System.out.printf("Prefix : %s %n", prefix);
        System.out.printf("Question Id : %s %n", ScyllaHelper.QUESTION_ID);
        System.out.printf("Quiz Id : %s %n", ScyllaHelper.QUIZ_ID);
        System.out.printf("Test Started : %s %n", start);
        System.out.printf("Test Ended : %s %n", end);
        System.out.printf("Total duration : %d %n", totalDuration);
        System.out.printf("Total Insert Count : %d %n", testInfo.insertCount);
        System.out.printf("Total Upsert Count : %d %n", testInfo.upsertCount);
        System.out.printf("Total Verify Count : %d %n", testInfo.verifyCount);
        System.out.printf("Total Upsert Success count : %d %n", testInfo.upsertSuccess);
        System.out.printf("Total Verify Success count : %d %n", testInfo.verifySuccess);
        System.out.printf("QPS : %d %n", totalDuration >0 ? (testInfo.upsertCount + testInfo.verifyCount + testInfo.insertCount) / totalDuration : (testInfo.upsertCount + testInfo.verifyCount + testInfo.insertCount));
        es.shutdown();
        cluster.close();
    }
}
